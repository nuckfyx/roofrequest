import Vue from 'vue'
import Vuex from 'vuex'
const fb = require('./firebaseConfig.js')


// test firebase connection
fb.usersCollection.get().then(snapshot => {
  snapshot.forEach(doc => {
    console.log( doc.data().name );
    console.log( doc.data().mail );
  });
});

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  }
})
