import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

// firebase init goes here
const config = {
    apiKey: "AIzaSyBnxOu0BkycPZg0H6xEX5tneSBNptDzXIM",
    authDomain: "roof-cbda1.firebaseapp.com",
    databaseURL: "https://roof-cbda1.firebaseio.com",
    projectId: "roof-cbda1",
    storageBucket: "roof-cbda1.appspot.com",
    messagingSenderId: "710391185771"
  }
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

// date issue fix according to firebase
const settings = {
    timestampsInSnapshots: true
}
db.settings(settings)

// firebase collections
const usersCollection = db.collection('user')


export {
    db,
    auth,
    currentUser,
    usersCollection
}
